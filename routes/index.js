var express = require("express");
var router = express.Router();
var { encrypt } = require("./../RSAExample");
var axios = require("axios");
var uuid = require("uuid");
const crypto = require("crypto");

const genHash = (partnerCode, partnerRefId, amount) => {
  let object = {
    partnerCode,
    partnerRefId,
    amount,
  };
  // let jsonString = JSON.stringify(object);
  let hash = encrypt(object);
  return hash;
};

const genSignature = ({
  partnerCode,
  partnerRefId,
  requestType,
  requestId,
  momoTransId,
}) => {
  var rawSignature =
    "partnerCode=" +
    partnerCode +
    "&partnerRefId=" +
    partnerRefId +
    "&requestType=" +
    requestType +
    "&requestType=" +
    requestId +
    "&requestId=" +
    momoTransId +
    "&momoTransId=" +
    momoTransId;
  var signature = crypto
    .createHmac("sha256", "K951B6PE1waDMi640xX08PD3vg6EkVlz")
    .update(rawSignature)
    .digest("hex");
  return signature;
};

router.get("/ping", async (req, res, next) => {
  // console.log(
  //   genHash(
  //     "MOMOPXBE20210507",
  //     "MOMOPXBE20210507-4lzrec74ei2kolhbmxn-235d9e73a641e3b41f65efc76e570832",
  //     50000
  //   )
  // );
  console.log(
    JSON.stringify({
      partnerCode: "MOMOPXBE20210507",
      partnerRefId:
        "MOMOPXBE20210507-4lzrec74ei2kolhbmxn-235d9e73a641e3b41f65efc76e570832",
      amount: 50000,
    })
  );
  res.send("pong");
});
router.post("/forwardPayment", async function (req, res, next) {
  let { appData, amount, customerNumber, orderId } = req.body;

  const dataRequestMomo = {
    partnerCode: "MOMOPXBE20210507",
    partnerRefId: orderId,
    customerNumber,
    appData,
    hash: genHash("MOMOPXBE20210507", orderId, amount),
    version: 2,
    payType: 3,
  };
  console.log(JSON.stringify(req.body, null, 2));
  console.log(JSON.stringify(dataRequestMomo, null, 2));
  // return res.send("ok");

  var config = {
    method: "post",
    url: "https://test-payment.momo.vn/pay/app",
    headers: {
      "Content-Type": "application/json",
    },
    data: dataRequestMomo,
  };
  axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
    })
    .catch(function (error) {
      console.log(error);
    });
});

router.post("/confirm", async function (req, res, next) {
  console.log(req.body, "bodybodybodybodybody");
  let { partnerCode, partnerRefId, momoTransId } = req.body;
  let requestId = uuid.v1();
  let requestType = "capture";

  const dataConfirmMoMo = {
    partnerCode,
    partnerRefId,
    requestType,
    requestId,
    momoTransId,
    signature: genSignature({
      partnerCode,
      partnerRefId,
      requestType,
      requestId,
      momoTransId,
    }),
  };

  console.log(dataConfirmMoMo, "dataConfirmMoModataConfirmMoMo");

  var config = {
    method: "post",
    url: "https://test-payment.momo.vn/pay/confirm",
    headers: {
      "Content-Type": "application/json",
    },
    data: dataConfirmMoMo,
  };
  axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data, null, 2));
    })
    .catch(function (error) {
      console.log(error);
    });

  res.status(200);
});
module.exports = router;
