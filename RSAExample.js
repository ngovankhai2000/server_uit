// npm install node-rsa
const NodeRSA = require("node-rsa");

// using your public key get from https://business.momo.vn/
// const fs = require("fs");
// const pubKey = fs.readFileSync("rsa.pub");
const pubKey = `-----BEGIN PUBLIC KEY-----
  MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEArpOqQpUFlEhjZoiDtZip4VaVcqz94Mlp34FfeIv8jLummcF+n69e446mQ9Z4EdWcH5XPwH89XOfENM1y+FulHRVUS0GfefkG8Fz3aa3MrnoHTVemjoXwyKKDYybPZyI0Wkh6prEih+nNN14ONxstSvp4npj3j8dyt41WUmSQk4hmZlvFqbfZLu6A99GoI8Fcgo0bxPUSNnRpPuZtp2dvYbQwIG441suC0Kio7RE3bQtCfMeKQpURh1k9zRy/AHc6ThhUEM4s/7CqXs9Qa2BL07CwELBGG6oqz8FSK3LeHqnvGCg2eTzlgyaPD42jKyL0cvAVm3mZMW9zYvxF8pA1ejiY5JUrp2tJOKq4K8x5D5lAypUQXplcolzAzW9U4PSKS/ZVgp+g+1vt8xiKBQLQfdASBXO173tZDHkcu6KPvyKWrUVA8PrvnMt5kBZiw4c/lXyFE5Bh9oog1hq1K8RcXNEW+GpHsv3DOcFXHkpN6GGmRnJ55p4ZQ2CGr1lT+p/CaAgv+L1llX+u8ZAmgiXfu8Nl2ZZTiVbeJJx3VoFVSZWTzkUr61DbXlgWIp4xkIQagyZmgu5IWu/5P7DYmByzsYYcg4XkXAuHuXp/epVjv8Tg+6q+e1vavGnMQqNbgUcOgR+MGOl1OGaR1iLtCP5o4x1wKEeiVq0Tbqvi3lh83csCAwEAAQ==
  -----END PUBLIC KEY-----`;

const key = new NodeRSA(pubKey, { encryptionScheme: "pkcs1" });
// const jsonData = {
//   "partnerCode": "MOMOV2OF20180515",
//   "partnerRefId": "caa5a630-8a3a-11e8-884c-653db95e86a6",
//   "amount": 500000,
//   "partnerTransId": "caa5a630-8a3a-11e8-884c-653db95e86a6"
// };

const encrypt = (jsonString) => {
  return key.encrypt(JSON.stringify(jsonString), "base64", "utf8");
};
module.exports = { encrypt };
// export default encrypt;
// const encrypted = key.encrypt(JSON.stringify(jsonData), 'base64');
// console.log('encrypted: ', encrypted);
